import json
from django.http import JsonResponse
from django.shortcuts import render
from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["style_name", "id", ]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["style_name", "id", "color", "fabric",]
    def get_extra_data(self, o):
        return {"location": o.location.href}

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        try:
            location = LocationVO.objects.get(href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    
@require_http_methods(["GET", "PUT", "DELETE"])
def api_hat_details(request, id):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=id)
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Hat does not exist"},
                status=400,
            )
        return JsonResponse(
            hat,
            HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=id)
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Hat does not exist"},
                status=400,
            )
        hat.delete()
        return JsonResponse(
                {"message": "Hat deleted"},
                status=200,
        )
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationVO.objects.get(href=content["location"])
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Location does not exist"},
                status=400,
            )
        Hat.objects.filter(id=id).update(**content)
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            HatDetailEncoder,
            safe=False,
        )