from .views import api_list_hats, api_hat_details
from django.urls import path

urlpatterns = [
    path("hats/", api_list_hats, name="hatslist"),
    path("hats/<int:id>/", api_hat_details, name="hatdetails"),
]