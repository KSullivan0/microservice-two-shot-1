from django.db import models

class LocationVO(models.Model):
    href = models.CharField(max_length=200)




class Hat(models.Model):

    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    location = models.ForeignKey(
        LocationVO,
        related_name = "hats",
        on_delete = models.CASCADE,
    )

    def __str__(self):
        return self.style_name

