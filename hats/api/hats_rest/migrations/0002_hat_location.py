# Generated by Django 4.0.3 on 2023-04-20 16:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='hat',
            name='location',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='hats', to='hats_rest.locationvo'),
            preserve_default=False,
        ),
    ]
