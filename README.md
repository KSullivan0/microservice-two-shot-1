# Wardrobify

Team:

* Kirk - Hats
* Maria - Shoes

## Design
Uses bootstrap card, container, col, and row elements to format pages

## Shoes microservice

The models in the shoes microservice are those of a Bin VO and a model for shoes.
The Bin VO keeps track of bin's capacity (as to not exceed when adding shoes),
    and href (to find the actual bin).
The shoe model keeps track of what bin the shoe is currently in using a foreign key
    to the bin VO.


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
