from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Shoe, BinVO
from common.json import ModelEncoder
import json


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture",
    ]
    def get_extra_data(self, o):
        return {"bin": o.bin.bin_href}
    
class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture",
    ]
    def get_extra_data(self, o):
        return {"bin": o.bin.bin_href}




@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all().order_by("bin_id")
        return JsonResponse(
            {"shoes": shoes},
            ShoeListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            bin = BinVO.objects.get(bin_href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Bin does not exist"},
                status=400,
            )
        if bin.size <= len(bin.shoes.all()):
            return JsonResponse(
                {"message": "Bin is full!"},
                status=200,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_shoe_details(request, id):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=id)
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Shoe does not exist"},
                status=400,
            )
        return JsonResponse(
            shoe,
            ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=id)
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Shoe does not exist"},
                status=400,
            )
        shoe.delete()
        return JsonResponse(
                {"message": "Shoe deleted"},
                status=200,
        )
    else:
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin = BinVO.objects.get(bin_href=content["bin"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Bin does not exist"},
                status=400,
            )
        Shoe.objects.filter(id=id).update(**content)
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            ShoeDetailEncoder,
            safe=False,
        )
        
