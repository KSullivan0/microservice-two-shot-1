import React , { useEffect, useState } from 'react';

function ShoeForm(props) {

    const [bins, setBins] = useState([]);
    const [alertClasses, setAlert] = useState("alert alert-warning d-none")
    const [shoeData, setShoeData] = useState({
        model_name: "",
        manufacturer: "",
        color: "",
        picture: "",
        bin: ""
    });
    let method = "post";

    const handleInput = async (e) => {
        setShoeData({
            ...shoeData,
            [e.target.name]: e.target.value,
        })
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const shoeUrl = "http://localhost:8080/api/shoes/"
        const fetchConfig = {
            method: method,
            body: JSON.stringify(shoeData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            if (newShoe.message === "Bin is full!") {
                setAlert("alert alert-warning");
            }
            setShoeData({
                model_name: "",
                manufacturer: "",
                color: "",
                picture: "",
                bin: ""
            });
        }
    };

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setBins(data.bins);
        }
    }
  
    useEffect(() => {fetchData();}, []);

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Shoe Data</h1>
                        <form onSubmit={handleSubmit} id="shoe-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleInput} value={shoeData.manufacturer} placeholder="" name="manufacturer" required type="text" id="manufacturer" className="form-control"/>
                                <label htmlFor="manufacturer">Manufacturer name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleInput} value={shoeData.model_name} placeholder="" name="model_name" required type="text" id="model_name" className="form-control"/>
                                <label htmlFor="model_name">Model name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleInput} value={shoeData.color} placeholder="" name="color" required type="text" id="color" className="form-control"/>
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleInput} value={shoeData.picture} placeholder="" name="picture" required type="text" id="picture" className="form-control"/>
                                <label htmlFor="picture">Picture</label>
                            </div>
                            <div className={alertClasses}>
                                Bin {shoeData.bin[shoeData.bin.length - 2]} is full!
                            </div>
                            <div className="mb-3">
                                <select onChange={handleInput} value={shoeData.bin} name="bin" id="bin" className="form-select" required>
                                    <option value="">Select a Bin</option>
                                    {bins.map(binObj => {
                                        if (binObj.bin_size) {
                                            return <option key={binObj.href} value={binObj.href}>{binObj.closet_name}: Bin {binObj.bin_number}</option>
                                        };
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Make changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ShoeForm;