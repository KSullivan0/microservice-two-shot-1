import React , { useEffect, useState } from 'react';

function HatForm(props) {

    const [locations, setLocations] = useState([]);
    const [hatData, setHatData] = useState({
        fabric: "",
        style_name: "",
        color: "",
        picture: "",
        location: ""
    });
    let alert_classes = "alert alert-warning d-none";
    let method = "post";

    const handleInput = async (e) => {
        setHatData({
            ...hatData,
            [e.target.name]: e.target.value,
        })
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const hatUrl = "http://localhost:8090/api/hats/"
        const fetchConfig = {
            method: method,
            body: JSON.stringify(hatData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            setHatData({
                fabric: "",
                style_name: "",
                color: "",
                picture: "",
                bin: ""
            });
        }
    };

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);
        }
    }
  
    useEffect(() => {fetchData();}, []);

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Hat Data</h1>
                        <form onSubmit={handleSubmit} id="hat-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleInput} value={hatData.fabric} placeholder="" name="fabric" required type="text" id="fabric" className="form-control"/>
                                <label htmlFor="fabric">Fabric type</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleInput} value={hatData.style_name} placeholder="" name="style_name" required type="text" id="style_name" className="form-control"/>
                                <label htmlFor="style_name">Style name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleInput} value={hatData.color} placeholder="" name="color" required type="text" id="color" className="form-control"/>
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleInput} value={hatData.picture} placeholder="" name="picture" required type="text" id="picture" className="form-control"/>
                                <label htmlFor="picture">Picture</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleInput} value={hatData.location} name="location" id="location" className="form-select" required>
                                    <option value="">Select a location</option>
                                    {locations.map(locationObj => {
                                    return <option key={locationObj.href} value={locationObj.href}>{locationObj.closet_name} Location {locationObj.location_number}</option>
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HatForm;